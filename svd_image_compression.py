import numpy as np
from scipy import linalg as la
from matplotlib import pyplot as plt

def svd_approx(A, k):
    '''
    Calculates the best rank k approximation to A with respect to the induced 
    2-norm. Uses the Singular Value Decomposition.
    Inputs:
        A -- array of shape (m,n)
        k -- positive integer
    Returns:
        Ahat -- best rank k approximation to A obtained via the SVD
    '''

    U,s,Vt = la.svd(A, full_matrices = False)
    S = np.diag(s[:k])
    A_hat = U[:,:k].dot(S).dot(Vt[:k,:])
    return A_hat

def lowest_rank_approx(A,e):
    '''
    Calculates the lowest rank approximation to A that has error strictly less 
    than e (in induced 2-norm).
    Inputs:
        A -- array of shape (m,n)
        e -- positive floating point number
    Returns:
        Ahat -- the best rank s approximation of A constrained to have error 
                less than e, where s is as small as possible.
    '''

    U,s,Vt = la.svd(A, full_matrices = False)
    indices = np.where(s < e)[0]
    #print e
    #print s
    #print indices
    if(indices.size < 1):
        rank = A.size
    else:
        rank = indices[0]
    return svd_approx(A, rank)

def plot_compressed_image(image_file, e):
    '''
    Reads the image at the given filepath.
    Shows the original image.
    Compresses the image, then shows the compression.
    '''
    img = plt.imread(image_file).astype(float)
    plt.imshow(img)
    plt.axis('off')
    plt.show()
    
    img[:,:,0] = lowest_rank_approx(img[:,:,0], e)
    img[:,0,:] = lowest_rank_approx(img[:,0,:], e)
    img[0,:,:] = lowest_rank_approx(img[0,:,:], e)

    plt.imshow(img)
    plt.axis('off')
    plt.show()
